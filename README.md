# jekyll-newsletter

Takes the latest post and sends it by email.

## Installation

Add this line to your site's Gemfile:

```ruby
gem 'jekyll-newsletter'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install jekyll-newsletter

## Usage

Add the plugin to your `_config.yml` and configure the email server.  If
you need a password for SMTP, you can also use environment variables.

```yaml
plugins:
- jekyll-newsletter
newsletter:
  from: Sutty <your+email@addre.ss>
  to: your+newsletter@addre.ss
  delivery_method: sendmail
  options:
    port: 25
    address: localhost
    enable_ssl: true
```

Environment variables:

```bash
JEKYLL_NEWSLETTER_USERNAME # Defaults to From address
JEKYLL_NEWSLETTER_PASSWORD
JEKYLL_NEWSLETTER_TO # If it's a secret address
JEKYLL_ENV=production # Anything else will just output the email
```

The `to` option can be a single address or an array.
For `JEKYLL_NEWSLETTER_TO` to have multiple addresses, use
a comma-separated list.

```yaml
newsletter:
  to:
  - some@addre.ss
  - another@o.ne
```

```bash
JEKYLL_NEWSLETTER_TO=some@addre.ss,another@o.ne
```

## Storage

To keep track of which posts have already been sent, this plugin can use
the Jekyll cache (`.jekyll-cache/`) so you'll need to keep it between
builds if you don't want to send duplicated newsletters.

For Netlify, you can use
[netlify-plugin-jekyll-cache](https://github.com/Mesomorphic/netlify-plugin-jekyll-cache).

If you enable the `jekyll-write-and-commit-changes` plugin, this
metadata will be stored on the posts themselves, but if you're using
a CI, these changes will be lost unless you push them back.


## Contributing

Bug reports and pull requests are welcome on 0xacab.org at
<https://0xacab.org/sutty/jekyll/jekyll-newsletter>. This project is
intended to be a safe, welcoming space for collaboration, and
contributors are expected to adhere to the [Sutty code of
conduct](https://sutty.nl/en/code-of-conduct/).

If you like our plugins, [please consider
donating](https://donaciones.sutty.nl/en/)!

## License

The gem is available as free software under the terms of the GPL3
License.

## Code of Conduct

Everyone interacting in the jekyll-newsletter project’s
codebases, issue trackers, chat rooms and mailing lists is expected to
follow the [code of conduct](https://sutty.nl/en/code-of-conduct/).
