# frozen_string_literal: true

require 'mail'

module Jekyll
  class Newsletter
    attr_reader :site

    CONFIG = {
      delivery_method: :logger,
      options: {
        user_name: ENV['JEKYLL_NEWSLETTER_USERNAME'],
        password: ENV['JEKYLL_NEWSLETTER_PASSWORD']
      }
    }.freeze

    def initialize(site)
      @site = site
    end

    def deliver
      return false unless post

      Jekyll.logger.info "Sending #{post.data['title']} as newsletter"

      return false unless mail.deliver

      Jekyll.logger.info 'Newsletter sent'

      cache[key_for_post(post)] = Time.now if production?

      true
    end

    # Select the newest Post that hasn't been sent already.
    # Jekyll::Collection doesn't guarantee order so we convert to
    # a Liquid drop to get the ordered Posts.
    def post
      @post ||= site.to_liquid['site']['posts'].find do |post|
        !cache[key_for_post(post)]
      rescue StandardError
        # When the post isn't in the cache
        true
      end
    end

    def mail
      @mail ||= begin
        mail = Mail.new
        mail.from      = config[:from]
        mail.to        = config[:to]
        mail.subject   = post.data['title']
        mail.html_part = post.content
        # TODO: We don't have access to Markdown on post_write because
        # Jekyll replaces the contents with the rendered output, but we
        # could keep *some* formatting.
        mail.text_part = post.content.gsub(/<[^>]+>/m, '')

        mail.delivery_method config[:delivery_method], **config[:options]

        mail
      end
    end

    def config
      @config ||= Jekyll::Utils.deep_merge_hashes(CONFIG,
                                                  (site.config['newsletter'] || {}).transform_keys(&:to_sym)).tap do |config|

        config[:to] = [config[:to]].flatten
        config[:to].concat(ENV['JEKYLL_NEWSLETTER_TO']&.split(',')&.map(&:strip) || [])
        config[:to].compact!

        %i[to from].each do |o|
          raise Jekyll::Errors::InvalidConfigurationError, "Missing `newsletter.#{o}` in _config.yml" if config[o].nil? || config[o].empty?
        end

        config[:delivery_method] = config[:delivery_method].to_sym
        config[:options].transform_keys!(&:to_sym)
        config[:options][:user_name] ||= config[:from].scan(/<([^>]+)>/)&.flatten&.first || config[:from]
        config[:delivery_method] = :logger unless production?
      end.freeze
    end

    private

    def production?
      Jekyll.env == 'production'
    end

    def key_for_post(post)
      post.data['uuid'] || post.relative_path
    end

    def cache
      @cache ||= Jekyll::Cache.new(self.class.to_s)
    end
  end
end
