# frozen_string_literal: true

require_relative 'jekyll/newsletter'

Jekyll::Hooks.register :site, :post_write do |site|
  newsletter = Jekyll::Newsletter.new site
  newsletter.deliver
end
